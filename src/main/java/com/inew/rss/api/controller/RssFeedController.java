package com.inew.rss.api.controller;

import static com.inew.rss.api.builder.GenericResponseBuilder.aGenericResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inew.rss.api.model.client.NewRequestDto;
import com.inew.rss.api.exception.RssServiceException;
import com.inew.rss.api.model.client.AnalyseResponse;
import com.inew.rss.api.model.client.GenericResponse;
import com.inew.rss.api.model.client.HotTopicResponse;
import com.inew.rss.api.service.RssFeedService;

@RestController
@RequestMapping(path = "")
public class RssFeedController {
	
	@Autowired
	private RssFeedService rssFeedService;
	
	@GetMapping(path = "/frequency/{id}")
	public ResponseEntity<?> getTopFrecuencyById(@PathVariable Long id) {

		try {
			
			// Analysis the new
			HotTopicResponse response = rssFeedService.getTopFrecuencyById(id);
			
			// Success response
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (RssServiceException e) {
			// Unsuccess response
			GenericResponse error = aGenericResponse()
					.withCode(e.getStatus().value())
					.withMessage(e.getMessage())
					.build();
			
			return new ResponseEntity<>(error, e.getStatus());
		}
	}
	
	@PostMapping(path = "/analyse/new")
	public ResponseEntity<?> analyseNew(@RequestBody NewRequestDto request) {
		try {
			
			// Analysis the new
			AnalyseResponse response = this.rssFeedService.analyseNew(request.getUrlList());
			
			// Success response
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (RssServiceException e) {
			// Unsuccess response
			GenericResponse error = aGenericResponse()
					.withCode(e.getStatus().value())
					.withMessage(e.getMessage())
					.build();
			
			return new ResponseEntity<>(error, e.getStatus());
		}
	}
	
}