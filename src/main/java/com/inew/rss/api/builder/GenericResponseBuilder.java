package com.inew.rss.api.builder;

import com.inew.rss.api.model.client.GenericResponse;

public class GenericResponseBuilder {

	private int code;
	private String message;
	
	private GenericResponseBuilder() {
		this.code = 200;
		this.message = null;
	}

	public GenericResponseBuilder withCode(int code) {
		this.code = code;
		return this;
	}

	public GenericResponseBuilder withMessage(String message) {
		this.message = message;
		return this;
	}

	public GenericResponse build() {
		return new GenericResponse(this.message, this.code);
	}
	
	public static GenericResponseBuilder aGenericResponse() {
		return new GenericResponseBuilder();
	}
	
}