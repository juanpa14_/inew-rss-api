package com.inew.rss.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.inew.rss.api.model.HotTopicSummary;

public interface IHotTopicSummaryRepository extends CrudRepository<HotTopicSummary, Long> {

	HotTopicSummary findByIdAndStatus(Long id, int status);
	
	@Query("SELECT e FROM HotTopicSummary e WHERE id = ?1 and status = 1 ")
	HotTopicSummary findSuccessById(Long id);
	
}
