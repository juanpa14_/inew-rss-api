package com.inew.rss.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.inew.rss.api.model.RssFeed;

@Repository
public interface IRssFeedRepository extends CrudRepository<RssFeed, Long> {

}