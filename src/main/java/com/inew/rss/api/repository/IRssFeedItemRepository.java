package com.inew.rss.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.inew.rss.api.model.HotTopicSummary;
import com.inew.rss.api.model.RssFeedItem;

public interface IRssFeedItemRepository extends CrudRepository<RssFeedItem, Long> {

	@Query("SELECT DISTINCT h.mainItem FROM HotTopicItem h "
			+ "WHERE h.summary = ?1 ")
	List<RssFeedItem> findDistMainsByHotTopic(HotTopicSummary summary);

	@Query("SELECT count(h.secItem) FROM HotTopicItem h "
			+ "WHERE h.summary = ?1 "
			+ "and h.mainItem = ?2 ")
	int totalSecsByHotTopicAndMain(HotTopicSummary summary, RssFeedItem main);

	RssFeedItem findByGuid(String guid);
	
}
