package com.inew.rss.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.inew.rss.api.model.HotTopicItem;
import com.inew.rss.api.model.HotTopicSummary;
import com.inew.rss.api.model.RssFeedItem;

public interface IHotTopicItemRepository extends CrudRepository<HotTopicItem, Long> {
	
	@Query("SELECT e FROM HotTopicItem e "
			+ "WHERE e.summary = ?1 "
			+ "and e.mainItem = ?2 "
			+ "and e.secItem = ?3")
	HotTopicItem findBySummaryAndMainItemAndSecItem(HotTopicSummary summary, RssFeedItem main, RssFeedItem sec);

}
