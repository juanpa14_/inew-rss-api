package com.inew.rss.api.service;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.inew.rss.api.enumerators.HotTopicSummaryStatus;
import com.inew.rss.api.exception.RssFeedUrlException;
import com.inew.rss.api.exception.RssServiceException;
import com.inew.rss.api.model.HotTopicItem;
import com.inew.rss.api.model.HotTopicSummary;
import com.inew.rss.api.model.RssFeed;
import com.inew.rss.api.repository.IHotTopicItemRepository;
import com.inew.rss.api.repository.IHotTopicSummaryRepository;
import com.inew.rss.api.repository.IRssFeedRepository;
import com.inew.rss.api.repository.IRssFeedItemRepository;
import com.inew.rss.api.model.RssFeedItem;
import com.inew.rss.api.model.client.AnalyseResponse;
import com.inew.rss.api.model.client.HotTopicItemResponse;
import com.inew.rss.api.model.client.HotTopicResponse;

@Service
public class RssFeedService {
	
	// Constants
	private static final String CHNL = "channel";
	private static final String COPY = "copyright";
	private static final String DESC = "description";
	private static final String GUID = "guid";
	private static final String ITEM = "item";
	private static final String LANG = "language";
	private static final String LINK = "link";
	private static final String TITLE = "title";
	
	// Attributes
	@Autowired
	private IHotTopicItemRepository iHotTopicItemRepository;
	@Autowired
	private IHotTopicSummaryRepository iHotTopicSummaryRepository;
	@Autowired
	private IRssFeedRepository iRssFeedRepository;
	@Autowired
	private IRssFeedItemRepository iRssFeedItemRepository;
	
	// Methods
	/**
	 * 
	 * @param urlList
	 * @return
	 * @throws RssServiceException
	 */
	public AnalyseResponse analyseNew(List<String> urlList) 
			throws RssServiceException {
		try {
			
			if (urlList == null || urlList.size() < 2) {
				throw new RssServiceException("At least there must be two sources to analysis.", HttpStatus.BAD_REQUEST);
			}
			
			// Gets the RSS Feeds from URL list
			List<RssFeed> rssList = this.getRssFeedsFromUrlList(urlList);
			
			// HotTopic data and its items
			HotTopicSummary summary = this.saveSummary();	
			List<HotTopicItem> items = buildHotTipicItems(rssList);

			// Saves the data of the hoot topic and its items
			saveHotTopicData(summary, items);
			
			// Success response
			return new AnalyseResponse(summary.getId());
			
		} catch (RssServiceException e) {
			throw e;
		} catch (RssFeedUrlException e) {
			throw new RssServiceException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (RuntimeException e) {
			throw new RssServiceException("Timeout error.", HttpStatus.GATEWAY_TIMEOUT);
		} catch (Exception e) {
			throw new RssServiceException("There was some trouble reading one of the sources.", HttpStatus.NOT_IMPLEMENTED);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws RssServiceException
	 */
	public HotTopicResponse getTopFrecuencyById(Long id) throws RssServiceException {
		
		try {
			// Searches the hot topic summary by the identifier
			HotTopicSummary summary = iHotTopicSummaryRepository.findSuccessById(id);
			
			// Validates the data of the hot topic
			if (summary == null) {
				throw new RssServiceException("Hot topics not found.", HttpStatus.NOT_FOUND);
			}
			
			// Top items
			int top = 3;

			// Top topic items
			List<HotTopicItemResponse> topItems = this.getTopTopicItems(summary, top);
			
			// Response
			return new HotTopicResponse(id, topItems);
		} catch (RssServiceException e) {
			throw e;
		} catch (Exception e) {
			throw new RssServiceException("There was an unespected error looking for the hot topics.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * 
	 * @param dic
	 * @param key
	 * @param value
	 */
	private void addElementToDic(Map<String, String> dic, String key, String value) {
		if (!dic.containsKey(key)) {
			dic.put(key, value);
		}
	}
	
	/**
	 * 
	 * @param rssList
	 */
	private List<HotTopicItem> buildHotTipicItems(List<RssFeed> rssList) {
		// List of item
		List<HotTopicItem> list = new ArrayList<>();
		
		//
		for (int i=0; i < rssList.size(); i ++) {
			
			for (int j = 0; j < rssList.size(); j++) {
				
				if (j != i) {
					this.compareRssFeeds(list, rssList.get(i), rssList.get(j));
				}
			}
		}
		
		return list;
	}
	
	/**
	 * 
	 * @param dic
	 * @return
	 */
	private RssFeed buildRssFeedFromDic(Map<String, String> dic) {
		
		// Validates the instance of the feed
		RssFeed rss = new RssFeed(
				dic.get(COPY),// copyright, 
				dic.get(DESC),// description, 
				dic.get(LANG),// language, 
				dic.get(LINK),// link, 
				dic.get(TITLE)// title
			);
		
		// Clear dictionary keys and values
		dic.clear();
		
		return rss;
	}
	
	/**
	 * 
	 * @param is
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private Node getChannel(InputStream is) 
			throws ParserConfigurationException, SAXException, IOException {
		
		// Factory
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    
	    // Element XML
		Document doc = builder.parse(is);
		Element element = doc.getDocumentElement();
		
		// Get all child nodes
		NodeList nodes = element.getChildNodes();
		
		// print the text content of each child
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeName().trim().equals(CHNL)) {
				return nodes.item(i);
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param node
	 * @return
	 */
	private String getNodeValue(Node node) {
		return node.hasChildNodes() ? node.getFirstChild().getNodeValue() : null;
	}
	
	/**
	 * 
	 * @param item
	 * @param rss
	 * @return
	 */
	private RssFeedItem buildRssItemFromNode(Node item, RssFeed rss) {
		// Children of the item
		NodeList nodes = item.getChildNodes();
		Map<String, String> dic = new HashMap<>();
		
		// Tags names of the RSS
		List<String> tags = Arrays.asList(DESC, GUID, LINK, TITLE);
		
		// Process each node
        for (int i = 0; i < nodes.getLength(); i++) {
        	if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
        		// Tag name
	        	String tag = nodes.item(i).getNodeName().trim();
	        	
	        	// Validates the tag is into the tags names of the RSS item
	        	if (tags.indexOf(tag) > -1) {
					addElementToDic(dic, tag, getNodeValue(nodes.item(i)));
				}
        	}
        }
        
		return this.buildRssFeetItemFromDic(dic, rss);
	}
	
	/**
	 * 
	 * @param channel
	 * @return
	 */
	private RssFeed buildRssFeedChannelNode(Node channel) {
		// Instance of elements
		RssFeed rss = null;
		List<RssFeedItem> items = new ArrayList<>();
		Map<String, String> dic = new HashMap<>();
		
		// Children of the channel
		NodeList nodes = channel.getChildNodes();
		
		// Tags names of the RSS
		List<String> tags = Arrays.asList(COPY, DESC, LANG, LINK, TITLE);

		// Process each node
        for (int i = 0; i < nodes.getLength(); i++) {
        	if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
        		// Tag name
	        	String tag = nodes.item(i).getNodeName().trim();
	        	// Validates if the tag is an item
	        	if(tag.equals(ITEM)) {

					// Builds the RSS if is null
					if (rss == null) {
						rss = this.buildRssFeedFromDic(dic);
					}
					items.add(this.buildRssItemFromNode(nodes.item(i), rss));
	        	}
	        	
	        	// Validates the tag is into the tags names of the RSS 
	        	else if (tags.indexOf(tag) > -1) {
					addElementToDic(dic, tag, getNodeValue(nodes.item(i)));
				}
        	}
        }
		
		// Sets the items of the RSS
		if (rss != null) {
			rss.getItems().addAll(items);
		}
        
        return rss;
	}
	
	/**
	 * 
	 * @param urlList
	 * @return
	 * @throws RssServiceException
	 * @throws RssFeedUrlException
	 * @throws IOException
	 * @throws ParserConfigurationException 
	 * @throws SAXException 
	 */
	private List<RssFeed> getRssFeedsFromUrlList(List<String> urlList) throws RssServiceException, RssFeedUrlException, IOException, ParserConfigurationException, SAXException {
		List<RssFeed> rssList = new ArrayList<>();
		
		// Variables to get the data from URL list
		InputStream is;

		// Process each URL
		for(String feedUrl : urlList) {
			is = this.readUrlData(this.readFeedUrl(feedUrl));
			// Node parent
			Node channel = this.getChannel(is);
			if (channel != null) {
				rssList.add(this.buildRssFeedChannelNode(channel));
			}
			is.close();
		}
		
		return rssList;
	}
	
	/**
	 * 
	 * @param dic
	 * @param rss
	 * @return
	 */
	private RssFeedItem buildRssFeetItemFromDic(Map<String, String> dic, RssFeed rss) {
		// Instances of an item
		RssFeedItem item = new RssFeedItem(
				dic.get(DESC),// description,
				dic.get(GUID),// guid,
				dic.get(LINK),// link,
				dic.get(TITLE)// title
			);
		
		item.setRssFeed(rss);
		
		// Clear dictionary keys and values
		dic.clear();
		
		return item;
	}
	
	/**
	 * 
	 * @param items
	 * @param rssItem
	 * @param rssCompare
	 */
	private void compareRssItems(List<HotTopicItem> items, RssFeedItem rssItem, RssFeed rssCompare) {
		
		for (RssFeedItem itemToCompare : rssCompare.getItems()) {
			if (this.isHotTopinBetweenItems(rssItem, itemToCompare)) {
				items.add(new HotTopicItem(null, rssItem, itemToCompare));
			}
		}
	}
	
	/**
	 * 
	 * @param items
	 * @param rss
	 * @param rssCompare
	 */
	private void compareRssFeeds(List<HotTopicItem> items, RssFeed rss, RssFeed rssCompare) {
		for(RssFeedItem item : rss.getItems()) {
			this.compareRssItems(items, item, rssCompare);
		}
	}

	/**
	 * 
	 * @param summary
	 * @return
	 */
	private List<HotTopicItemResponse> getFrequencyItems(HotTopicSummary summary) {
		// Items
		List<HotTopicItemResponse> items = new ArrayList<>();
		
		// Main it
		List<RssFeedItem> mains = this.iRssFeedItemRepository.findDistMainsByHotTopic(summary);
		
		for(RssFeedItem main : mains) {
			int secs = this.iRssFeedItemRepository.totalSecsByHotTopicAndMain(summary, main);
			items.add(
					new HotTopicItemResponse(
							main.getGuid(), // guid
							main.getLink(), // link
							main.getTitle(), // title
							secs // frequency
						)
				);
		}
		
		return items;
	}
	
	/**
	 * 
	 * @param summary
	 * @param top
	 * @return
	 */
	private List<HotTopicItemResponse> getTopTopicItems(HotTopicSummary summary, int top) 
			throws RssServiceException {
		// Topic items
		List<HotTopicItemResponse> topItems = this.getFrequencyItems(summary);

		// Validates the items
		if (topItems.isEmpty()) {
			throw new RssServiceException("Hot topics not found.", HttpStatus.NOT_FOUND);
		}

		// Limit of the list of the hot topic items
		int limit = topItems.size() > top ? top : topItems.size();

		// Top N (limit) of the hot topics items
		topItems = topItems.stream()
			.sorted(comparing(HotTopicItemResponse::getTotalFrequency).reversed())
			.limit(limit)
			.collect(toList());
		
		return topItems;
	}
	
	/**
	 * 
	 * @param rssItemA
	 * @param rssItemB
	 * @return
	 */
	private boolean isHotTopinBetweenItems(RssFeedItem rssItemA, RssFeedItem rssItemB) {
		// Reserved words
		List<String> reserved =  Arrays.asList(
				"for", "to", "a", "an", "between", "some", "any", 
				"why", "what", "who", "when", "the", "are", "you", 
				"we", "us", "they", "so", "then", "he", "she", "it",
				"them", "their", "her", "him", "his", "then", "must",
				"have", "be", "is", "being"
			);
		
		// List of words from de items titles
		String[] wordsA = rssItemA.getTitle().toLowerCase().split(" ");
		String[] wordsB = rssItemB.getTitle().toLowerCase().split(" ");
		
		// Searches coincidences
		for (String word : wordsA) {
			for (String wordB : wordsB) {
				 if (reserved.indexOf(word) < 0 && reserved.indexOf(wordB) < 0 
						 && wordB.startsWith(word) || word.startsWith(wordB)) {
					 return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param feedUrl
	 * @return
	 * @throws RssFeedUrlException
	 */
	private URL readFeedUrl(String feedUrl) throws RssFeedUrlException {
		try {
			return new URL(feedUrl);
		} catch (Exception e) {
			throw new RssFeedUrlException(feedUrl);
		}
	}
	
	/**
	 * 
	 * @param url
	 * @return
	 */
	private InputStream readUrlData(URL url) throws RssServiceException {
		try {
            return url.openStream();
        } catch (IOException e) {
			throw new RssServiceException("Timeout error.", HttpStatus.GATEWAY_TIMEOUT);
        }
	}
	
	/**
	 * 
	 * @param summary
	 * @param items
	 * @return
	 * @throws RssServiceException
	 */
	private void saveHotTopicData(HotTopicSummary summary, List<HotTopicItem> items) throws RssServiceException {
		try {
			
			// Save the items of the hot topic
			for(HotTopicItem item : items) {
				item.setSummary(summary);
				this.saveHotTopicItem(item);
			}
			
			this.saveSuccessSummary(summary);
		} catch (Exception e) {
			throw new RssServiceException("It was an unexpected error saving the data.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * 
	 * @param item
	 */
	private void saveHotTopicItem(HotTopicItem item) {
		// Save the rss items
		RssFeedItem main = this.saveRssFeedItem(item.getMainItem());
		RssFeedItem sec = this.saveRssFeedItem(item.getSecItem());

		this.saveHotTopicItem(item.getSummary(), main, sec);
		this.saveHotTopicItem(item.getSummary(), sec, main);
	}
	
	/**
	 * 
	 * @param summary
	 * @param main
	 * @param sec
	 */
	private void saveHotTopicItem(HotTopicSummary summary, RssFeedItem main, RssFeedItem sec) {
		// Validates if the item was registered
		if (summary.getId() != null && main.getId() != null && sec.getId() != null) {
			HotTopicItem item = this.iHotTopicItemRepository
					.findBySummaryAndMainItemAndSecItem(summary, main, sec);
			
			if (item != null) {
				return;
			}
		}
		
		// Builds entity data of the hot topic item
		HotTopicItem entity = new HotTopicItem();
		entity.setMainItem(main);
		entity.setSecItem(sec);
		entity.setSummary(summary);
		
		this.iHotTopicItemRepository.save(entity);
		
	}
	
	/**
	 * 
	 * @return
	 */
	private HotTopicSummary saveSummary() {
		// Saves the hot topic
		HotTopicSummary summary = new HotTopicSummary();
		return this.iHotTopicSummaryRepository.save(summary);
	}
	
	/**
	 * 
	 * @return
	 */
	private HotTopicSummary saveSuccessSummary(HotTopicSummary summary) {
		// Saves the hot topic
		summary.setStatus(HotTopicSummaryStatus.SUCCESS.getValue());
		return this.iHotTopicSummaryRepository.save(summary);
	}
	
	/**
	 * 
	 * @param rss
	 * @return
	 */
	private RssFeed saveRssFeed(RssFeed rss) {
		// Validates if the rss exists
		if (rss.getId() != null) {
			Optional<RssFeed> data = iRssFeedRepository.findById(rss.getId());
			if (data.isPresent()) {
				return data.get();
			}
		}
		
		// Saves the rss
		return this.iRssFeedRepository.save(rss);
	}
	
	/**
	 * 
	 * @param rssItem
	 * @return
	 */
	private RssFeedItem saveRssFeedItem(RssFeedItem rssItem) {
		// Validates if the item exists by its identifier
		if (rssItem.getId() != null) {
			Optional<RssFeedItem> data = this.iRssFeedItemRepository.findById(rssItem.getId());
			if (data.isPresent()) {
				return data.get();
			}
		}

		// Validates if the item exists by its guid
		if (rssItem.getGuid() != null) {
			RssFeedItem data = this.iRssFeedItemRepository.findByGuid(rssItem.getGuid());
			if (data != null) {
				return data;
			}
		}
		
		// Saves the rss feed of the item
		RssFeed rss = this.saveRssFeed(rssItem.getRssFeed());
		rssItem.setRssFeed(rss);
		
		// Saves the rss item
		return this.iRssFeedItemRepository.save(rssItem);
	}

}