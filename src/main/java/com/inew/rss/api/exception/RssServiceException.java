package com.inew.rss.api.exception;

import org.springframework.http.HttpStatus;

public class RssServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5933349921004569842L;
	
	HttpStatus status;

	public RssServiceException(String message, HttpStatus status) {
		super(message);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}
	
}
