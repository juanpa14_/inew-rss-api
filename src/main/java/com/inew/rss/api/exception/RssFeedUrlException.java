package com.inew.rss.api.exception;

import static java.lang.String.format;

public class RssFeedUrlException extends Exception {
	
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 5903706095606068174L;

	public RssFeedUrlException(String url) {
		super(format("It was not possible read/access the source: '%s'", url));
	}

}
