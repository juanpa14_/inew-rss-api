package com.inew.rss.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InewRssApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InewRssApiApplication.class, args);
	}

}
