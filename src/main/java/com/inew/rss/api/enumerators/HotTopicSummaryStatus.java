package com.inew.rss.api.enumerators;

public enum HotTopicSummaryStatus {

	// Values
	INITIAL (0),
	SUCCESS (1);
	
	// Attributes
	private int value;
	
	// Constructor
	HotTopicSummaryStatus(int value) {
		this.value = value;
	}

	// Methods
	public int getValue() {
		return value;
	}
	
}
