package com.inew.rss.api.model;

import static java.lang.String.format;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "RSS_FEED_ITEM")
public class RssFeedItem {
	
	// Attributes
	@Id
	@Column(name = "RSS_FEED_ITEM_ID", length = 10)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RSS_FEED_ITEM_SEQ")
    @SequenceGenerator(sequenceName = "rss_feed_item_seq", allocationSize = 1, name = "rss_feed_item_sec")
	private Long id;

	@Column(name = "DESCRIPTION", length = 7500, nullable = true, unique = false)
	private String description;

	@Column(name = "GUID", length = 500, nullable = true)
	private String guid;

	@Column(name = "LINK", length = 500, nullable = true, unique = false)
	private String link;

	@Column(name = "TITLE", length = 400, nullable = false, unique = false)
	private String title;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RSS_FEED_ID")
    private RssFeed rssFeed;
	
	@OneToMany(mappedBy="mainItem")
    private List<HotTopicItem> mains;
	
	@OneToMany(mappedBy="secItem")
    private List<HotTopicItem> secundaries;

	// Constructor
    public RssFeedItem() {
    	
    }
    
    public RssFeedItem(String description, String guid, String link, String title) {
    	this.description = description;
    	this.guid = guid;
    	this.link = link;
    	this.title = title;
    }

	// Methods
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public RssFeed getRssFeed() {
		return rssFeed;
	}

	public void setRssFeed(RssFeed rssFeed) {
		this.rssFeed = rssFeed;
	}

	public List<HotTopicItem> getMains() {
		return mains;
	}

	public void setMains(List<HotTopicItem> mains) {
		this.mains = mains;
	}

	public List<HotTopicItem> getSecundaries() {
		return secundaries;
	}

	public void setSecundaries(List<HotTopicItem> secundaries) {
		this.secundaries = secundaries;
	}

	@Override
    public String toString() {
    	return format("RssFeedItem [title=%s, description=%s, link=%s]",
    			this.title, this.description, this.link);
    }
    
}
