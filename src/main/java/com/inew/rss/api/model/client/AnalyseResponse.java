package com.inew.rss.api.model.client;

public class AnalyseResponse {

	// Attributes
	private Long code;

	// Constructor
	public AnalyseResponse( ) {
		
	}
	
	public AnalyseResponse(Long code) {
		this.code = code;
	}

	// Methods
	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
}
