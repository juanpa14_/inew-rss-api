package com.inew.rss.api.model;

import static java.lang.String.format;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.util.ArrayList;

@Entity
@Table(name = "RSS_FEED")
public class RssFeed {

	// Attributes
	@Id
	@Column(name = "RSS_FEED_ID", length = 10)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RSS_FEED_SEQ")
    @SequenceGenerator(sequenceName = "rss_feed_seq", allocationSize = 1, name = "rss_feed_sec")
	private Long id;

	@Column(name = "COPYRIGHT", length = 500, nullable = true, unique = false)
	private String copyright;
	
	@Column(name = "DESCRIPTION", length = 3500, nullable = true, unique = false)
	private String description;

	@Column(name = "LANGUAGE", length = 200, nullable = true, unique = false)
	private String language;

	@Column(name = "LINK", length = 500, nullable = true, unique = false)
    private String link;

	@Column(name = "TITLE", length = 400, nullable = false, unique = false)
	private String title;
	
	@OneToMany(mappedBy="rssFeed", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<RssFeedItem> items = new ArrayList<RssFeedItem>();

	// Constructor
	public RssFeed() {
		
	}

    public RssFeed(String copyright, String description, String language,
    		String link, String title) {
        this.copyright = copyright;
        this.description = description;
        this.language = language;
        this.link = link;
        this.title = title;
    }

	// Methods
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<RssFeedItem> getItems() {
		return items;
	}

	public void setItems(List<RssFeedItem> items) {
		this.items = items;
	}

	@Override
    public String toString() {
    	return format("RssFeed [copyright=%s, description=%s, language=%s, link=%s, title=%s]",
    			this.copyright, this.description, this.language, this.link, this.title);
    }

}
