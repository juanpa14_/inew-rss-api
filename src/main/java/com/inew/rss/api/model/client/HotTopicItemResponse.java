package com.inew.rss.api.model.client;

public class HotTopicItemResponse {

	// Attributes
	private String guid;
	private String link;
	private String title;
	private int totalFrequency;
	
	// Constructor
	public HotTopicItemResponse() {
		
	}
	
	public HotTopicItemResponse(String guid, String link, String title, int totalFrequency) {
		this.guid = guid;
		this.link = link;
		this.title = title;
		this.totalFrequency = totalFrequency;
	}

	// Methods
	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTotalFrequency() {
		return totalFrequency;
	}

	public void setTotalFrequency(int totalFrequency) {
		this.totalFrequency = totalFrequency;
	}
	
}
