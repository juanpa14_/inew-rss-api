package com.inew.rss.api.model.client;

public class GenericResponse {
	
	private int code;
	private String message;
	
	protected GenericResponse() {
		
	}
	
	public GenericResponse(String message, int code) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}