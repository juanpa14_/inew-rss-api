package com.inew.rss.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "HOT_TOPIC_ITEM")
public class HotTopicItem {
	
	// Attributes
	@Id
	@Column(name = "HOT_TOPIC_ITEM_ID", length = 10)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HOT_TOPIC_ITEM_SEQ")
    @SequenceGenerator(sequenceName = "hot_topic_item_seq", allocationSize = 1, name = "hot_topic_item_sec")
	private Long id;

    @ManyToOne
    @JoinColumn(name="HOT_TOPIC_SUMMARY_ID", nullable=false)
    private HotTopicSummary summary;

    @ManyToOne
    @JoinColumn(name="main_item", nullable=false)
    private RssFeedItem mainItem;

    @ManyToOne
    @JoinColumn(name="sec_item", nullable=false)
    private RssFeedItem secItem;

	// Constructor
	public HotTopicItem() {
		
	}
	
	public HotTopicItem(HotTopicSummary summary, RssFeedItem mainItem, RssFeedItem secItem) {
		this.mainItem = mainItem;
		this.secItem = secItem;
		this.summary = summary;
	}

	// Methods
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public HotTopicSummary getSummary() {
		return summary;
	}

	public void setSummary(HotTopicSummary summary) {
		this.summary = summary;
	}

	public RssFeedItem getMainItem() {
		return mainItem;
	}

	public void setMainItem(RssFeedItem mainItem) {
		this.mainItem = mainItem;
	}

	public RssFeedItem getSecItem() {
		return secItem;
	}

	public void setSecItem(RssFeedItem secItem) {
		this.secItem = secItem;
	}

}
