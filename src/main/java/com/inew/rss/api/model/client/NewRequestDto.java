package com.inew.rss.api.model.client;

import java.util.List;

// import com.fasterxml.jackson.annotation.JsonProperty;
// import javax.validation.constraints.NotNull;
// import org.springframework.validation.annotation.Validated;

public class NewRequestDto {
	
	private List<String> urlList;
	
	protected NewRequestDto() {
		
	}
	
	public NewRequestDto(List<String> urlList) {
		this.urlList = urlList;
	}

	public List<String> getUrlList() {
		return urlList;
	}

	public void setUrlList(List<String> urlList) {
		this.urlList = urlList;
	}
	
}