package com.inew.rss.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.inew.rss.api.enumerators.HotTopicSummaryStatus;

@Entity
@Table(name = "HOT_TOPIC_SUMMARY")
public class HotTopicSummary {
	
	// Attributes
	@Id
	@Column(name = "HOT_TOPIC_SUMMARY_ID", length = 10)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HOT_TOPIC_SUMM_SEQ")
    @SequenceGenerator(sequenceName = "hot_topic_summ_seq", allocationSize = 1, name = "hot_topic_summ_sec")
	private Long id;
	
	@Column(name = "STATUS", length = 1)
	private int status;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(
		name="HOT_TOPIC_ITEM",
		joinColumns=@JoinColumn(name="HOT_TOPIC_SUMMARY_ID"),
		inverseJoinColumns=@JoinColumn(name="HOT_TOPIC_ITEM_ID")
	)
	private List<HotTopicItem> items;

	// Constructor
	public HotTopicSummary() {
		this.status = HotTopicSummaryStatus.INITIAL.getValue();
	}

	// Methods
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<HotTopicItem> getItems() {
		return items;
	}

	public void setItems(List<HotTopicItem> items) {
		this.items = items;
	}
	
}
