package com.inew.rss.api.model.client;

import java.util.List;

public class HotTopicResponse {

	// Attributes
	private Long code;
	private List<HotTopicItemResponse> items;
	
	// Constructor
	public HotTopicResponse( ) {
		
	}
	
	public HotTopicResponse(Long code, List<HotTopicItemResponse> items) {
		this.code = code;
		this.items = items;
	}

	// Methods
	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public List<HotTopicItemResponse> getItems() {
		return items;
	}

	public void setItems(List<HotTopicItemResponse> items) {
		this.items = items;
	}
	
}
