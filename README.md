
## API Definition: 

```
/analyse/new
```

### API Input:

{
	"urlList" : [
		"https://www.feedforall.com/sample-feed.xml",
		"https://www.feedforall.com/sample.xml",
		"https://blogs.nasa.gov/stationreport/feed/",
		"https://news.google.com/rss?cf=all&pz=1&hl=en-US&gl=US&ceid=US:en"
	]
}

Where, ***urlList*** is a list of RSS URLs

### API Response:

{
    "code": 1
}

Where, ***code*** is the identifier of the analyse

## API Definition: 

```
/frequency/{id}
```

### API Input:

This API endpoint takes an id as input

### API Output:

{
    "code": 1,
    "items": [
        {
            ...
        },
        {
            ...
        },
        {
            ...
        }
    ]
}

## Additional Information

Running the exercise with maven

```mvn spring-boot:run```

Or running the exercise with maven wrapper

```mvnw spring-boot:run```
